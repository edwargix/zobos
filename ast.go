package main

import (
	"fmt"
)

func (node *ParseTreeNode) indexInParentChildren() int {
	for i, child := range node.parent.children {
		if child == node {
			return i
		}
	}
	panic(fmt.Errorf("node not in parent's children"))
}

func (node *ParseTreeNode) replaceWith(other *ParseTreeNode) {
	other.parent = node.parent
	node.parent.children[node.indexInParentChildren()] = other
}

func (node *ParseTreeNode) delete() {
	i := node.indexInParentChildren()
	node.parent.children = append(node.parent.children[:i],
		node.parent.children[i+1:]...)
}

func (node *ParseTreeNode) astChildren() {
	for _, c := range node.children {
		c.concrete2ast()
	}
}

func (node *ParseTreeNode) removeChild(i int) {
	node.children = append(node.children[:i], node.children[i+1:]...)
}

func (node *ParseTreeNode) transferChild(i int, other *ParseTreeNode) {
	node.children[i].parent = other
	other.children = append(other.children, node.children[i])
	node.removeChild(i)
}

func (node *ParseTreeNode) liftChildrenOfChild(i int) {
	node.children = append(node.children[:i],
		append(node.children[i].children, node.children[i+1:]...)...)
}

func (node *ParseTreeNode) concrete2ast() {
	switch node.symbol {
	case "PROGRAM":  // STMTS $
		node.children[0].concrete2ast()
		node.removeChild(1)
		node.liftChildrenOfChild(0)
	case "STMTS":
		// STMTS -> STMTS STATEMENT
		// STMTS -> lambda
		switch node.children[0].symbol {
		case "lambda":
			node.replaceWith(node.children[0])
		case "STMTS":
			node.astChildren()
			if node.children[0].symbol == "lambda" {
				node.removeChild(0)
			} else {  // STMTS
				node.transferChild(1, node.children[0])
				node.replaceWith(node.children[0])
			}
		}
	case "IF":
		// IF -> if lparen BEXPR rparen STATEMENT
		node.astChildren()
		node.removeChild(3)
		node.removeChild(1)
		node.removeChild(0)
	case "IFELSE":
		// IFELSE -> if lparen BEXPR rparen BRACESTMTS else STATEMENT
		node.astChildren()
		node.children = []*ParseTreeNode{node.children[2], node.children[4], node.children[6]}
	case "BRACESTMTS":
		// BRACESTMTS -> lbrace STMTS rbrace
		node.astChildren()
		node.children = []*ParseTreeNode{node.children[1]}
		node.liftChildrenOfChild(0)
	case "STATEMENT":
		// STATEMENT -> BRACESTMTS
		// STATEMENT -> DECLLIST sc
		// STATEMENT -> ASSIGN sc
		// STATEMENT -> IF
		// STATEMENT -> IFELSE
		// STATEMENT -> WHILE
		// STATEMENT -> EMIT sc
		node.astChildren()
		if node.children[0].symbol == "BRACESTMTS" {
			node.replaceWith(node.children[0])
		} else {
			if len(node.children) > 1 {
				node.removeChild(1)
			}
		}
	case "DECLLIST":
		// DECLLIST -> DECLTYPE DECLIDS
		node.astChildren()
		for len(node.children[1].children) > 0 {
			node.children[1].transferChild(0, node)
		}
		node.removeChild(1)
	case "DECLTYPE":
		// DECLTYPE -> const bool
		// DECLTYPE -> bool
		// DECLTYPE -> const int
		// DECLTYPE -> int
		// DECLTYPE -> string
		// DECLTYPE -> const float
		// DECLTYPE -> float
		node.astChildren()
		sym := ""
		for i, c := range node.children {
			if i > 0 {
				sym += " "
				sym += c.symbol
			} else {
				sym += c.symbol
			}
		}
		node.symbol = sym
		node.children = nil
	case "DECLIDS":
		// DECLIDS -> DECLID
		// DECLIDS -> DECLIDS comma DECLID
		node.astChildren()
		switch len(node.children) {
		case 1:
		case 3:
			node.transferChild(2, node.children[0])
			node.replaceWith(node.children[0])
		}
	case "DECLID":
		// DECLID -> id
		// DECLID -> ASSIGN
		node.astChildren()
	case "EXPR":  // BEXPR | AEXPR
		node.children[0].concrete2ast()
		node.replaceWith(node.children[0])
	case "BEXPR":  // AEXPR BOOLS AEXPR
		node.astChildren()
		node.transferChild(0, node.children[1])
		node.transferChild(1, node.children[0])  // shift
		node.replaceWith(node.children[0])
	case "AEXPR":  // SUM
		node.children[0].concrete2ast()
		node.replaceWith(node.children[0])
	case "SUM":
		// SUM PLUS PRODUCT
		// PRODUCT
		switch node.children[0].symbol {
		case "SUM":
			node.astChildren()
			node.transferChild(0, node.children[1])
			node.transferChild(1, node.children[0])  // shift
			node.replaceWith(node.children[0])
		case "PRODUCT":
			node.children[0].concrete2ast()
			node.replaceWith(node.children[0])
		}
	case "PLUS":  // plus | minus
		node.children[0].concrete2ast()
		node.replaceWith(node.children[0])
	case "PRODUCT":
		// PRODUCT TIMES VALUE
		// VALUE
		switch node.children[0].symbol {
		case "VALUE":
			node.children[0].concrete2ast()
			node.replaceWith(node.children[0])
		case "PRODUCT":
			node.astChildren()
			node.transferChild(0, node.children[1])
			node.transferChild(1, node.children[0])  // shift
			node.replaceWith(node.children[0])
		}
	case "TIMES":  // mult | div | mod
		node.children[0].concrete2ast()
		node.replaceWith(node.children[0])
	case "VALUE":
		// VALUE -> lparen AEXPR rparen
		// VALUE -> lparen BEXPR rparen
		// VALUE -> intval
		// VALUE -> floatval
		// VALUE -> stringval
		// VALUE -> id
		// VALUE -> UNARY
		// VALUE -> CAST
		switch node.children[0].symbol {
		case "lparen":
			node.children[1].concrete2ast()
			node.replaceWith(node.children[1])
		case "UNARY", "CAST", "intval", "floatval", "stringval", "id":
			node.children[0].concrete2ast()
			node.replaceWith(node.children[0])
		}
	case "intval", "floatval", "stringval", "id":
		// noop
	case "BOOLS":
		// BOOLS -> lt
		// BOOLS -> leq
		// BOOLS -> eq
		// BOOLS -> geq
		// BOOLS -> gt
		node.children[0].concrete2ast()
		node.replaceWith(node.children[0])
	case "UNARY":
		// UNARY -> PLUS VALUE
		// UNARY -> not VALUE
		// UNARY -> compl VALUE
		node.astChildren()
		node.transferChild(1, node.children[0])
		node.replaceWith(node.children[0])
	case "CAST":
		// CAST -> bool lparen AEXPR rparen
		// CAST -> int lparen AEXPR rparen
		// CAST -> float lparen AEXPR rparen
		node.astChildren()
		node.transferChild(2, node.children[0])
		node.replaceWith(node.children[0])
	case "ASSIGN":
		// ASSIGN -> id assign EXPR
		// ASSIGN -> id assign ASSIGN
		node.astChildren()
		node.transferChild(0, node.children[1])
		node.transferChild(1, node.children[0])  // shift
		node.replaceWith(node.children[0])
	case "WHILE":
		// WHILE -> while lparen BEXPR rparen STATEMENT
		node.astChildren()
		node.removeChild(3)
		node.removeChild(1)
		node.removeChild(0)
	case "EMIT":
		// EMIT -> emit id AEXPR AEXPR
		// EMIT -> emit symtable
		node.astChildren()
		node.removeChild(0)
	default:
		// TODO: debug flag
		// fmt.Println(node.symbol)
		if node.data != "" {
			node.symbol = node.data
			node.data = ""
		}
		for _, c := range node.children {
			c.concrete2ast()
		}
	}
}
