package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"unicode"
	"strconv"
)

type Token struct {
	id     string
	val    string
	lineno int
	colno  int
}

func readTokenStreamFile(f *os.File) []Token {
	tokens := []Token{}
	lineScanner := bufio.NewScanner(f)

	for lineScanner.Scan() {
		parts := strings.FieldsFunc(lineScanner.Text(), unicode.IsSpace)
		if len(parts) == 0 {
			continue
		}
		if len(parts) != 4 {
			panic(fmt.Errorf("every line in the token stream file"+
				"must have four fields"))
		}
		// handle alphabet encoding in val
		val := ""
		for i := 0; i < len(parts[1]); i++ {
			if parts[1][i] == 'x' {
				c, err := strconv.ParseInt(parts[1][i+1:i+3], 16, 8)
				if err != nil {
					panic(err)
				}
				val += string(byte(c))
				i += 2
			} else {
				val += string(parts[1][i])
			}
		}
		lineno, err := strconv.Atoi(parts[2])
		if err != nil {
			panic(err)
		}
		colno, err := strconv.Atoi(parts[3])
		if err != nil {
			panic(err)
		}
		tokens = append(tokens, Token{
			id:     parts[0],
			val:    val,
			lineno: lineno,
			colno:  colno,
		})
	}

	return tokens
}
