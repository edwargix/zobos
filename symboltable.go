package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func (ast *ParseTreeNode) analyzeSymbols(symbolTableFile *os.File) int {
	ret := 0
	writer := bufio.NewWriter(symbolTableFile)

	st := NewSymbolTable()

	var recur func(*ParseTreeNode)
	recurChildren := func(node *ParseTreeNode) {
		for _, c := range node.children {
			recur(c)
		}
	}
	// returns set vars and used vars
	var getIDs func(*ParseTreeNode) ([]*ParseTreeNode, []*ParseTreeNode)
	getIDs = func(node *ParseTreeNode) ([]*ParseTreeNode, []*ParseTreeNode) {
		setVars := make([]*ParseTreeNode, 0)
		usedVars := make([]*ParseTreeNode, 0)
		if node.symbol == "=" {
			idNode := node.children[0]
			for {
				setVars = append(setVars, idNode)
				if idNode.parent.children[1].symbol == "=" {
					idNode = idNode.parent.children[1].children[0]
				} else {
					node = idNode.parent.children[1]
					break
				}
			}
		}
		if node.symbol == "id" {
			usedVars = append(usedVars, node)
		}
		for _, child := range node.children {
			newSetVars, newUsedVars := getIDs(child)
			usedVars = append(usedVars, append(newSetVars, newUsedVars...)...)
			setVars = append(setVars, newSetVars...)
		}
		return setVars, usedVars
	}

	var getType func(*ParseTreeNode) (string, []*ParseTreeNode)
	getType = func(node *ParseTreeNode) (string, []*ParseTreeNode) {
		switch node.symbol {
		case "=":
			return getType(node.children[1])
		case ">=", "<=", ">", "<", "==":
			left, lErrors := getType(node.children[0])
			right, rErrors := getType(node.children[1])
			if len(lErrors) > 0 || len(rErrors) > 0 {
				return "", append(lErrors, rErrors...)
			}
			if left == "string" || right == "string" ||
				left == "bool" || right == "bool" {
				return "", []*ParseTreeNode{node}
			}
			return "bool", nil
		case "+", "-", "*", "/":
			switch len(node.children) {
			case 1:
				T, errs := getType(node.children[0])
				if len(errs) > 0 {
					return T, errs
				}
				if T == "string" || T == "bool" {
					return "", []*ParseTreeNode{node}
				}
				return T, nil
			case 2:
				left, lErrors := getType(node.children[0])
				right, rErrors := getType(node.children[1])
				switch {
				case len(lErrors) > 0 || len(rErrors) > 0:
					return "", append(lErrors, rErrors...)
				case (left == "float" && right == "int") ||
					(left == "int" && right == "float"):
					return "float", nil
				case left == "float" && right == "float":
					return "float", nil
				case left == "int" && right == "int":
					return "int", nil
				default:
					return "", []*ParseTreeNode{node}
				}
			default:
				panic("+ expressions must have 1 or 2 children")
			}
		case "~":
			T, errs := getType(node.children[0])
			if len(errs) > 0 {
				return T, errs
			}
			if T != "int" {
				return "", []*ParseTreeNode{node}
			}
			return T, nil
		case "%":
			left, lErrors := getType(node.children[0])
			right, rErrors := getType(node.children[0])
			switch {
			case len(lErrors) > 0 || len(rErrors) > 0:
				return "", append(lErrors, rErrors...)
			case left == "int" && right == "int":
				return "int", nil
			default:
				return "", []*ParseTreeNode{node}
			}
		case "!":
			T, errs := getType(node.children[0])
			switch {
			case len(errs) > 0:
				return T, errs
			case T == "bool":
				return "bool", nil
			default:
				return "", []*ParseTreeNode{node}
			}
		case "string":
			T, errs := getType(node.children[0])
			if len(errs) > 0 {
				return T, errs
			}
			return "string", nil
		case "stringval":
			return "string", nil
		case "float":
			T, errs := getType(node.children[0])
			if len(errs) > 0 {
				return T, errs
			}
			return "float", nil
		case "floatval":
			return "float", nil
		case "bool":
			return "bool", nil
		case "int":
			T, errs := getType(node.children[0])
			if len(errs) > 0 {
				return T, errs
			}
			return "int", nil
		case "intval":
			return "int", nil
		case "id":
			if sym := st.retrieveSymbol(node.data); sym == nil {
				return "", []*ParseTreeNode{node}
			} else {
				found := false
				for depth := st.depth; depth >= 0; depth-- {
					if _, ok := sym.init[depth]; ok {
						found = true
						break
					}
				}
				if !found {
					fmt.Printf("OUTPUT :WARN: %d %d :UNINIT:\n",
						node.lineno, node.colno)
				}
				if strings.HasPrefix(sym.T, "const ") {
					return sym.T[len("const "):], nil
				} else {
					return sym.T, nil
				}
			}
		default:
			panic(fmt.Errorf("%s not handled for expression types", node.symbol))
		}
	}

	var checkConv func(string, string, bool) bool
	checkConv = func(setT string, storeT string, first bool) bool {
		switch setT {
		case "string":
			return storeT != "int" && storeT != "float" && storeT != "bool"
		case "float":
			return storeT != "int" && storeT != "bool" && storeT != "string"
		case "bool":
			return storeT != "float" && storeT != "string"
		case "int":
			return storeT != "bool" && storeT != "string"
		case "const string":
			return first && checkConv("string", storeT, false)
		case "const float":
			return first && checkConv("float", storeT, false)
		case "const bool":
			return first && checkConv("bool", storeT, false)
		case "const int":
			return first && checkConv("int", storeT, false)
		default:
			panic(fmt.Errorf("%s type not handle in checkConv", setT))
		}
	}

	handleAssigns := func(root *ParseTreeNode, storeT string) {
		setVars, usedVars := getIDs(root)
		for _, usedVar := range usedVars {
			if sym := st.retrieveSymbol(usedVar.data); sym != nil {
				sym.used = true
			}
		}
		if storeT == "" {
			usedVars = append(setVars, usedVars...)
		}
		noVar := false
		for _, idNode := range usedVars {
			if st.retrieveSymbol(idNode.data) == nil {
				fmt.Printf("OUTPUT :ERROR: %d %d :NOVAR:\n",
					idNode.lineno, idNode.colno)
				ret = -1
				noVar = true
			}
		}
		if storeT != "" {
			for _, idNode := range setVars {
				st.enterSymbol(idNode.data, storeT, idNode.lineno, idNode.colno)
			}
		}
		setT, errs := getType(root)
		if !noVar {
			for _, err := range errs {
				fmt.Printf("OUTPUT :ERROR: %d %d :EXPR:\n", err.lineno, err.colno)
				ret = -1
			}
		}
		if len(errs) == 0 && !noVar {
			for _, idNode := range setVars {
				if sym := st.retrieveSymbol(idNode.data); sym != nil {
					sym.init[st.depth] = struct{}{}  // this var has been declared in this scope
					if storeT == "" && strings.HasPrefix(sym.T, "const") {
						fmt.Printf("OUTPUT :WARN: %d %d :CONST:\n", idNode.lineno, idNode.colno)
					}
					if !checkConv(setT, sym.T, storeT != "") {
						fmt.Printf("OUTPUT :ERROR: %d %d :CONV:\n",
							// idNode.parent is the "="
							idNode.parent.lineno, idNode.parent.colno)
						ret = -1
					}
				}
			}
		} else {
			for _, idNode := range setVars {
				if sym := st.retrieveSymbol(idNode.data); sym != nil {
					sym.init[st.depth] = struct{}{}  // this var has been declared in this scope
				}
			}
		}
	}

	recur = func(node *ParseTreeNode) {
		switch node.symbol {
		case "EMIT":
			switch node.children[0].symbol {
			case "symtable":
				for scope, syms := range st.scopeDispaly {
					for sym := syms; sym != nil; sym = sym.level {
						writer.WriteString(fmt.Sprintf("%d,%s,%s\n",
							scope, sym.T, sym.name))
					}
				}
			case "id":
				for _, child := range node.children {
					handleAssigns(child, "")
				}
			default:
				panic("bad EMIT child")
			}
		case "DECLLIST":
			storeT := node.children[0].symbol
			for _, child := range node.children[1:] {
				val := child.children[0]
				switch val.symbol {
				case "id":
					st.enterSymbol(val.data, storeT, val.lineno, val.colno)
				case "=":
					handleAssigns(val, storeT)
				}
			}
		case "PROGRAM":
			recurChildren(node)
		case "STATEMENT":
			child := node.children[0]
			switch child.symbol {
			case "=":
				handleAssigns(child, "")
			default:
				recur(child)
			}
		case "BRACESTMTS":
			st.openScope()
			recurChildren(node)
			st.closeScope()
		case "IF", "WHILE":
			handleAssigns(node.children[0], "")
			recur(node.children[1])
		case "IFELSE":
			handleAssigns(node.children[0], "")
			recur(node.children[1])
			recur(node.children[2])
		default:
			panic(fmt.Errorf("%s (%s) not handled", node.symbol, node.data))
		}
	}
	recur(ast)

	st.warnUnusedVars()  // for global scope

	writer.Flush()

	return ret
}

type SymbolTable struct {
	depth        int
	hashTable    map[string]*Symbol
	scopeDispaly []*Symbol
}

type Symbol struct {
	name   string
	T      string
	depth  int
	v      *Symbol
	level  *Symbol
	lineno int
	colno  int
	used   bool
	init   map[int]struct{}  // add key with scope depth to say this var was declared
}

func NewSymbolTable() *SymbolTable {
	return &SymbolTable{
		depth: 0,
		hashTable: make(map[string]*Symbol),
		scopeDispaly: make([]*Symbol, 1),
	}
}

func (st *SymbolTable) openScope() {
	st.depth++
	st.scopeDispaly = append(st.scopeDispaly, nil)
}

func (st *SymbolTable) closeScope() {
	st.warnUnusedVars()
	for sym := st.scopeDispaly[st.depth]; sym != nil; sym = sym.level {
		st.hashTable[sym.name] = sym.v
	}
	st.scopeDispaly = st.scopeDispaly[:st.depth]
	st.depth--
}

func (st *SymbolTable) warnUnusedVars() {
	for sym := st.scopeDispaly[st.depth]; sym != nil; sym = sym.level {
		if !sym.used {
			fmt.Printf("OUTPUT :WARN: %d %d :UNUSED:\n", sym.lineno, sym.colno)
		}
	}
}

func (st *SymbolTable) retrieveSymbol(name string) *Symbol {
	if sym, ok := st.hashTable[name]; ok {
		return sym
	} else {
		return nil
	}
}

func (st *SymbolTable) enterSymbol(name string, T string, lineno int, colno int) {
	// fmt.Printf("%s %s %d %d %d\n", name, T, lineno, colno, st.depth)
	if name == "" {
		panic("empty symbol name")
	}
	if T == "" {
		panic("empty symbol type")
	}
	oldsym := st.retrieveSymbol(name)
	if oldsym != nil && oldsym.depth == st.depth {
		fmt.Printf("OUTPUT :WARN: %d %d :REVAR:\n", lineno, colno)
		return
	}
	newsym := &Symbol{
		name:   name,
		T:      T,
		depth:  st.depth,
		v:      oldsym,
		level:  st.scopeDispaly[st.depth],
		lineno: lineno,
		colno:  colno,
		used:   false,
		init:   make(map[int]struct{}),
	}
	st.hashTable[name] = newsym
	st.scopeDispaly[st.depth] = newsym
}
