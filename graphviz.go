package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func escape(s string) string {
	return strings.ReplaceAll(strings.ReplaceAll(s, "\\", "\\\\"),
		"\"", "\\\"")
}

func (tree *ParseTreeNode) draw(fname string) {
	file, err := os.Create(fname)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	writer := bufio.NewWriter(file)

	var recur func(node *ParseTreeNode)
	recur = func(node *ParseTreeNode) {
		if node.data == "" {
			fmt.Fprintf(writer, "\"%p\" [label=\"%s\"]\n", node, escape(node.symbol))
		} else {
			// fmt.Fprintf(writer, "\"%p\" [label=\"%s (%s)\"]\n", node,
			// 	escape(node.symbol),
			// 	escape(node.data))
			fmt.Fprintf(writer, "\"%p\" [label=\"%s\"]\n", node, escape(node.data))
		}
		for _, child := range node.children {
			fmt.Fprintf(writer, "\"%p\" -> \"%p\"\n", node, child)
			recur(child)
		}
	}

	writer.WriteString("digraph {\n")
	recur(tree)
	writer.WriteString("}\n")
	writer.Flush()
}
