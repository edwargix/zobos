GOSRC != find . -name '*.go'
GOSRC += go.mod
GO = go
RM ?= rm -f
GV_SOURCES = $(wildcard *.gv)
PNGS = $(GV_SOURCES:.gv=.png)

ZOBOS: $(GOSRC)
	$(GO) build -o $@

run: ZOBOS
	./ZOBOS ./zobos/tests/onecast.tok ast.dat symtable.dat
	make pngs

grader: zobos-student.tar.bz2
	tar xvjf $<

%.png: %.gv
	dot -Tpng $< -o $@

pngs: $(PNGS)

clean:
	$(RM) -r ZOBOS *.gv *.png *.tt zobos/ scan.u

davidflorness.tar.gz: Makefile $(GOSRC) zlang.lr zlang.cfg
	git archive -o $@ HEAD $^
