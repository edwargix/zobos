package main

import (
	"log"
	"os"
)

func usage() {
	log.Fatalf("usage: %s token_stream_file ast_file symbol_table_file",
		os.Args[0])
}

func main() {
	if len(os.Args) < 3 {
		usage()
	}

	tokenStreamFile, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer tokenStreamFile.Close()
	tokens := readTokenStreamFile(tokenStreamFile)

	symbolTableFile, err := os.Create(os.Args[3])
	if err != nil {
		log.Fatal(err)
	}
	defer symbolTableFile.Close()

	zlangLRFile, err := os.Open("zlang.lr")
	if err != nil {
		log.Fatal(err)
	}
	defer zlangLRFile.Close()
	lrTable := readLRFile(zlangLRFile)

	zlangCFGFile, err := os.Open("zlang.cfg")
	if err != nil {
		log.Fatal(err)
	}
	defer zlangCFGFile.Close()
	cfgProductions := readCFGFile(zlangCFGFile)

	tree := LRParse(lrTable, tokens, cfgProductions)

	// tree.draw("concrete.gv")
	tree.concrete2ast()
	tree.draw(os.Args[2])
	os.Exit(tree.analyzeSymbols(symbolTableFile))
}
