package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
	"unicode"
)

type Production struct {
	lhs string
	rhs []string
}

type LRTable []map[string]*LRAction

type LRAction struct {
	action string
	num    int
}

type ParseTreeNode struct {
	symbol   string
	children []*ParseTreeNode
	data     string
	parent   *ParseTreeNode
	lineno   int
	colno    int
}

type lrStackItem struct {
	tree  *ParseTreeNode
	state int
}

func LRParse(T LRTable, ts []Token, P []Production) *ParseTreeNode {
	S := []lrStackItem{lrStackItem{nil, 0}}

	Q := []*ParseTreeNode{}
	for _, t := range ts {
		Q = append(Q, &ParseTreeNode{symbol: t.id, data: t.val,
			lineno: t.lineno, colno: t.colno})
	}

	reduce := func(productionNum int) *ParseTreeNode {
		production := P[productionNum]
		node := &ParseTreeNode{
			symbol:   production.lhs,
			children: make([]*ParseTreeNode, len(production.rhs)),
		}
		for i := len(node.children)-1; i >=0; i-- {
			if production.rhs[i] == "lambda" {
				node.children[i] = &ParseTreeNode{symbol: "lambda", parent: node}
			} else {
				S[0].tree.parent = node
				node.children[i] = S[0].tree
				S = S[1:]
			}
		}
		return node
	}

	lineno := Q[0].lineno
	colno := Q[0].colno

	for {
		if len(Q) == 0 {
			Q = append(Q, &ParseTreeNode{symbol: "$"})
		}
		t := Q[0]
		if t.lineno > lineno || (t.lineno >= lineno && t.colno > colno) {
			lineno = t.lineno
			colno = t.colno
		}
		lrAction := T[S[0].state][t.symbol]
		if lrAction == nil {
			fmt.Printf("OUTPUT :SYNTAX: %d %d :SYNTAX:\n", lineno, colno)
			os.Exit(-1)
		}
		switch lrAction.action {
		case "sh":  // shift
			S = append([]lrStackItem{lrStackItem{t, lrAction.num}}, S...)
			Q = Q[1:]
		case "r":  // reduce
			Q = append([]*ParseTreeNode{reduce(lrAction.num)}, Q...)
		case "R":  // reduce and accept
			return reduce(lrAction.num)
		}
	}
}

func readLRFile(f *os.File) LRTable {
	lineScanner := bufio.NewScanner(f)

	table := make(LRTable, 0)
	symbols := make([]string, 0)

	lineScanner.Scan()

	// read first line
	parts := strings.FieldsFunc(lineScanner.Text(), func(c rune) bool {
		return c == ','
	})
	if len(parts) == 0 {
		panic(fmt.Errorf(
			"the first line of the SLR file cannot be empty"))
	}
	for _, p := range parts[1:] {  // 0th element is a useless placeholder
		if p == "" {
			panic(fmt.Errorf("cannot have empty grammar symbol"))
		} else {
			symbols = append(symbols, p)
		}
	}

	for lineScanner.Scan() {
		parts := strings.Split(lineScanner.Text(), ",")
		row := make(map[string]*LRAction)
		for i, p := range parts[1:] {
			if p != "" {
				action := strings.Split(p, "-")
				num, err := strconv.Atoi(action[1])
				if err != nil {
					panic(err)
				}
				row[symbols[i]] = &LRAction{
					action[0],
					num,
				}
			}
		}
		table = append(table, row)
	}

	return table
}

func readCFGFile(f *os.File) []Production {
	productions := []Production{Production{}}
	lineScanner := bufio.NewScanner(f)

	var lhs string
	for lineScanner.Scan() {
		line := lineScanner.Text()
		if len(line) == 0 || line[0] == '#' {  // comment
			continue
		}
		parts := strings.FieldsFunc(line, unicode.IsSpace)
		var i, j int
		if parts[0] == "|" {
			if lhs == "" {
				panic(fmt.Errorf(
					"came across alternation with no "+
					"preceding LHS symbol in CFG file",
				))
			}
			if parts[1] == "|" {
				panic(fmt.Errorf(
					"cannot begin RHS of production with |"))
			}
			j = 1
			i = 2
		} else {
			lhs = parts[0]
			if parts[1] != "->" {
				panic(fmt.Errorf(
					"production must have \"->\" after LHS"))
			}
			if parts[2] == "|" {
				panic(fmt.Errorf(
					"cannot begin RHS of production with |"))
			}
			// start at 2 to skip "->"
			j = 2
			i = 3
		}
		for ; i < len(parts); i++ {
			if parts[i] == "|" {
				productions = append(productions, Production{
					lhs: lhs,
					rhs: parts[j:i],
				})
				j = i+1
			}
		}
		productions = append(productions, Production{
			lhs: lhs,
			rhs: parts[j:i],
		})
	}

	return productions
}
